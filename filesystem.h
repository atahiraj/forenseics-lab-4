
#ifndef _FILESYSTEM_H_
#define _FILESYSTEM_H_

#include <types.h>

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

struct fileSystem {
    struct fat32BS bs;
    offset_t partition;
    offset_t fat;
    int fd;
};

static void init_fs(struct fileSystem *fs, const int fd)
{
    fs->fd = fd;
    fs->partition = lseek(fd, 0, SEEK_CUR);
    unsigned long ret = read(fd, &fs->bs, FAT32_BS_SIZE);
    if (ret < FAT32_BS_SIZE) {
        fprintf(stderr, "Failed to read boot sector\n");
        exit(1);
    }
    fs->fat = fs->partition + fs->bs.BPB_RsvdSecCnt * fs->bs.BPB_BytsPerSec;
}

static cluster_t next_cluster(const struct fileSystem *fs, const cluster_t N) 
{
    const offset_t position = lseek(fs->fd, 0, SEEK_CUR);
    cluster_t buffer;
    lseek(fs->fd, fs->fat + (N * 4), SEEK_SET);
    read(fs->fd, &buffer, 4);
    lseek(fs->fd, position, SEEK_SET);
    return buffer;
}

static sector_t cluster_sector(const struct fileSystem *fs, const cluster_t N) 
{
    return fs->bs.BPB_RsvdSecCnt + fs->bs.BPB_NumFATs * fs->bs.BPB_FATSz32 + (N - 2) * fs->bs.BPB_SecPerClus;
}

static offset_t sector_offset(const struct fileSystem *fs, const sector_t N)
{
    return N * fs->bs.BPB_BytsPerSec + fs->partition;
}

static cluster_t offset_cluster(const struct fileSystem *fs, const offset_t N)
{
    unsigned long bpcluster = fs->bs.BPB_SecPerClus * fs->bs.BPB_BytsPerSec;
    offset_t first_data = fs->partition + (fs->bs.BPB_RsvdSecCnt + fs->bs.BPB_NumFATs * fs->bs.BPB_FATSz32) * fs->bs.BPB_BytsPerSec;
    return 2 + (N - first_data) / bpcluster;
}

#endif // _FILESYSTEM_H_
