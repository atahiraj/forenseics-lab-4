#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdint.h>

#include <unistd.h>
#include <fcntl.h>

#include <types.h>
#include <filesystem.h>

#define PARTITION_TYPE 0x0C
#define SECTOR_SIZE 512

static inline void remove_spaces(unsigned char * dest, const unsigned char * src, const unsigned n)
{
    for (unsigned i = 0; i < n; ++i)
        dest[i] = src[i] == ' ' ? '\0' : src[i];
}

static inline void sanitize_file(char * dest, const char * file)
{
    strcpy(dest, file);
    if(strcmp(dest, ".") && strcmp(dest, ".."))
        for (unsigned i = 0; i < strlen(dest); ++i)
            dest[i] = dest[i] == '.' ? '\0' : dest[i];
}

static offset_t cluster_entry_offset(const struct fileSystem *fs, const cluster_t cluster, const char* entry_name)
{
    
    const unsigned dir_per_cluster = fs->bs.BPB_SecPerClus * fs->bs.BPB_BytsPerSec / FAT32_DIR_SIZE;

    const sector_t sector = cluster_sector(fs, cluster);
    lseek(fs->fd, sector_offset(fs, sector), SEEK_SET);
    for (unsigned i = 0; i < dir_per_cluster; ++i) {
        struct fat32Dir dir;
        read(fs->fd, &dir, FAT32_DIR_SIZE);

        // Removing spaces
        unsigned char name[12];
        remove_spaces(name, dir.DIR_Name, 11);
        name[11] = '\0';

        if (name[0] == 0xE5)
            continue;
        else if(name[0] == 0x00)
            break;
        else if (!strcasecmp(entry_name, (char*) name))
            return lseek(fs->fd, 0, SEEK_CUR) - FAT32_DIR_SIZE;
    }
    return 0;
}

static offset_t entry_offset(const struct fileSystem *fs, cluster_t cluster, const char *entry_name)
{
    offset_t entry = 0;
    do
    {
        entry = cluster_entry_offset(fs, cluster, entry_name);
        cluster = next_cluster(fs, cluster);
    } while ( (entry == 0) && ( (cluster & 0x0FFFFFFF) < 0x0FFFFFF8) );
    
    return entry;
}

static offset_t file_offset(const struct fileSystem *fs, char * path)
{
    cluster_t cluster = fs->bs.BPB_RootClus;

    char *token = strtok(path, "/");
    offset_t file = 0;
    do {        
        char sanitized_token[12];
        sanitize_file(sanitized_token, token);
        file = entry_offset(fs, cluster, sanitized_token);
        if(!file)
            break;

        struct fat32Dir dir;
        lseek(fs->fd, file, SEEK_SET);
        read(fs->fd, &dir, FAT32_DIR_SIZE);
        
        cluster = ((dir.DIR_FstClusHI << 16) + dir.DIR_FstClusLO);
        cluster = cluster ? cluster : 2; // if token == '..' == root
    } while( (token = strtok(NULL, "/")) );

    return file;
}

static void read_slack(const struct fileSystem *fs, offset_t file)
{
    lseek(fs->fd, file, SEEK_SET);
    struct fat32Dir dir;
    read(fs->fd, &dir, FAT32_DIR_SIZE);

    byte4_t file_size = dir.DIR_FileSize;
    
    byte4_t cluster_size = fs->bs.BPB_BytsPerSec * fs->bs.BPB_SecPerClus;

    // No slack to print
    if(file_size % cluster_size == 0)
        return;

    cluster_t cluster = ((dir.DIR_FstClusHI << 16) + dir.DIR_FstClusLO);
    for (unsigned i = 0; i < file_size / cluster_size; ++i)
        cluster = next_cluster(fs, cluster);

    offset_t cluster_offset = sector_offset(fs, cluster_sector(fs, cluster));
    offset_t slack = file_size % cluster_size + cluster_offset;

    byte4_t slack_size = cluster_size - file_size % cluster_size;
    printf("%lx\n", slack);
    printf("%d\n", slack_size);
    lseek(fs->fd, slack, SEEK_SET);

    for(unsigned i = 0; i < slack_size; ++i)
    {
        char buffer;
        read(fs->fd, &buffer, 1);
        printf("%c", buffer);
    }
}

int main(int argc, char *argv[]) {
    // Reading arguments
    if (argc < 4) {
        fprintf(stderr, "Missing arguments\n");
        exit(1);
    }

    const char * if_arg = strtok(argv[1], "=");
    const char * input_file = strtok(NULL, "=");
    if (strncmp(if_arg, "if", 2)) {
        fprintf(stderr, "Invalid argument: %s\n", if_arg);
        exit(1);
    }
    const char * part_arg = strtok(argv[2], "=");
    const char * part_nb_str = strtok(NULL, "=");
    const int part_nb = atoi(part_nb_str);
    if (strncmp(part_arg, "part", 4)) {
        fprintf(stderr, "Invalid argument: %s\n", part_arg);
        exit(1);
    } else if (part_nb > 3 || part_nb < 0) {
        fprintf(stderr, "Partition should be between 0 and 3\n");
        exit(1);
    }
    char * query_file = argv[3];

    const int fd = open(input_file, O_RDONLY);
    if (fd == -1) {
        fprintf(stderr, "Failed to open file %s: ", input_file);
        perror(NULL);
        exit(1);
    }

    // Reading MBR
    struct MBR mbr;
    unsigned long int ret = read(fd, &mbr, MBR_SIZE);
    if (ret < MBR_SIZE) {
        fprintf(stderr, "Failed to read MBR\n");
        exit(1);
    }
    
    const struct partitionEntry *pe = &mbr.partition_entries[part_nb];
	if(pe->partition_type != PARTITION_TYPE){
		fprintf(stderr, "Unsupported file system type: %02x\n", pe->partition_type);  
		exit(1);
	}

    // Initializing the filesystem
    lseek(fd, pe->lba * SECTOR_SIZE, SEEK_SET);
    struct fileSystem fs;
    init_fs(&fs, fd);

    
    offset_t file = file_offset(&fs, query_file);

    read_slack(&fs, file);
    return 0;
}
