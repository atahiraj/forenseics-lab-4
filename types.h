
#ifndef _TYPES_H_
#define _TYPES_H_

#define MBR_SIZE sizeof(struct MBR)
#define FAT32_BS_SIZE sizeof(struct fat32BS)
#define FAT32_DIR_SIZE sizeof(struct fat32Dir)

typedef unsigned long int offset_t; 
typedef unsigned int sector_t;
typedef unsigned int cluster_t;

typedef unsigned char byte_t;
typedef unsigned short byte2_t;
typedef unsigned int byte4_t;
typedef unsigned long byte8_t;

struct __attribute__((__packed__)) CHSAddress{
	byte_t head;
	byte_t sector_cylinder_high_bits;
	byte_t cylinder_low_bits;
};

struct __attribute__((__packed__)) partitionEntry{
	byte_t status;
	struct CHSAddress start;
	unsigned char partition_type;
	struct CHSAddress end;
	byte4_t lba;
	byte4_t num_sectors;
};

struct __attribute__((__packed__)) MBR {
    byte_t unused_area[446];
    struct partitionEntry partition_entries[4];
    byte_t signature[2];
};

struct __attribute__((__packed__)) fat32BS {
    byte_t BS_jmpBoot[3];
    unsigned char BS_OEMName[8];
    byte2_t BPB_BytsPerSec;
    byte_t BPB_SecPerClus;
    byte2_t BPB_RsvdSecCnt;
    byte_t BPB_NumFATs;
    byte2_t BPB_RootEntCnt;
    byte2_t BPB_TotSec16;
    byte_t BPB_Media;
    byte2_t BPB_FATSz16;
    byte2_t BPB_SecPerTrk;
    byte2_t BPB_NumHeads;
    byte4_t BPB_HiddSec;
    byte4_t BPB_TotSec32;
    byte4_t BPB_FATSz32;
    byte2_t BPB_ExtFlags;
    byte2_t BPB_FSVer;
    byte4_t BPB_RootClus;
    byte2_t BPB_FSInfo;
    byte2_t BPB_BkBootSec;
    byte_t BPB_Reserved[12];
    byte_t BS_DrvNum;
    byte_t BS_Reserved1;
    byte_t BS_BootSig;
    byte4_t BS_VolID;
    unsigned char BS_VolLab[11];
    unsigned char BS_FilSysType[8];
    byte_t unused_area[420];
    byte_t signature[2];
};

struct __attribute__((__packed__)) fat32Dir {
    unsigned char DIR_Name[11];
    byte_t DIR_Attr;
    byte_t DIR_NTRes;
    byte_t DIR_CrtTimeTenth;
    byte2_t DIR_CrtTime;
    byte2_t DIR_CrtDate;
    byte2_t DIR_LstAccDate;
    byte2_t DIR_FstClusHI;
    byte2_t DIR_WrtTime;
    byte2_t DIR_WrtDate;
    byte2_t DIR_FstClusLO;
    byte4_t DIR_FileSize;
};

#endif // _TYPES_H_
